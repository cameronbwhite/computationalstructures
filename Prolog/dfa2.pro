% Sipser EX.1.7
% M is the DFA (Q, E, D, q, F)
% M = ({1,2}, {0,1},
%      {(1,0,1),(1,1,2),(2,0,1),(2,1,2)},
%      1, {2})

parse(L) :-
    start(S),
    trans(S,L).

trans(X, []) :-
    final(X).
trans(X, [A|As]) :-
    delta(X, A, X1),
    trans(X1, As).

start(1).
final(2).

delta(1, 0, 1).
delta(1, 1, 2).
delta(2, 0, 1).
delta(2, 1, 2).
