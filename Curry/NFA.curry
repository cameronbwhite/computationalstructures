type NFA q s = {
    states  :: [q],
    symbols :: [s],
    delta   :: q -> s -> [q],
    start   :: q,
    final   :: [q]
    }

path nfa ss | qs =:= deltabar nfa q0 ss = qs
              where q0 = (nfa :> start)
                    qs = q0 : xs ++ [f]
                    f = foldl1 (?) (nfa :> final)
                    xs free

deltabar _   q []     = [q]
deltabar nfa q (s:ss) = q : deltabar nfa q' ss
    where q' = foldl1 (?) $ (nfa :> delta) q s


n1 = { states  := "ABCD", 
       symbols := "01", 
       delta   := delta', 
       start   := 'A', 
       final   := "D"  }
delta' 'A' '0' = "BC"
delta' 'B' '0' = "C" 
delta' 'B' '1' = "D" 
delta' 'C' '0' = "C" 
delta' 'C' '1' = "D" 
