% N is the FST (Q, E, G, D, q)
% N = ({1,2,3}, {a,b}, {0,1},
%      {(1,a,2,1),(1,b,3,1),(2,a,3,1),
%       (2,b,1,0),(3,a,1,0),(3,b,2,1)}, 
%      1)

parse(I,O) :-
    start(S),
    trans(S,I,O).

trans(X, [], []).
trans(X, [I|Is], [O|Os]) :-
    delta(X, I, X1, O),
    trans(X1, Is, Os).

start(1).
 
% s0, i, s1, o
delta(1, a, 2, 1).
delta(1, b, 3, 1).
delta(2, a, 3, 1).
delta(2, b, 1, 0).
delta(3, a, 1, 0).
delta(3, b, 2, 1).
