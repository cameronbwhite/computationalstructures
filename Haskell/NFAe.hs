-- Copyright © 2013 Cameron White 
-- All rights reserved.
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the project nor the names of its contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
module NFAe (NFAe(NFAe), states, symbols, delta, start,
           final, path, paths, accept, union, cat, star,
           intersection, makeNFAe) where
    
import Text.Printf (printf, IsChar)
import Data.Maybe (fromJust, isNothing, isJust)
import Data.Set (Set)
import qualified Data.Set as Set

data NFAe q s = NFAe {
    states  :: Set q,
    symbols :: Set s,
    delta   :: q -> Maybe s -> Set q,
    start   :: q,
    final   :: Set q
    }

makeNFAe :: (Ord q, Ord s) => [q] -> [s] -> (q -> Maybe s -> Set q) 
                              -> q -> [q] -> NFAe q s
makeNFAe states' symbols' delta' start' final' = 
    NFAe (Set.fromList states') (Set.fromList symbols') 
         delta' start' (Set.fromList final')

instance (Show q, Show s) => Show (NFAe q s) where
    show (NFAe states symbols delta start final) =
        printf "(Q=%s,Σ=%s,δ=%s,q=%s,F=%s)" 
            (show states') (show symbols') 
            (show relations) (show start) (show final')
            where states' = Set.toList states
                  symbols' = Set.toList symbols
                  final' = Set.toList final
                  symbols'' = Nothing : map (Just) symbols' 
                  relations = [(q, showS s, delta' q s)| q<-states', s<-symbols'']
                  delta' q s = Set.toList $ delta q s
                  showS s | isNothing s = "ε"
                          | otherwise = show $ fromJust s

path :: Ord q => NFAe q s -> [s] -> [q]
path nfa ss | null qss = []
            | otherwise = head qss
    where qss = paths nfa ss

paths :: Ord q => NFAe q s -> [s] -> [[q]]
paths nfa ss = paths' nfa (start nfa) ss

paths' :: Ord q => NFAe q s -> q -> [s] -> [[q]]
paths' nfa q [] =
    [Set.toList $ (Set.singleton q) `Set.intersection` (final nfa)]

paths' nfa q w@(s:ss) = map (q:) qss''
    where q1s   = (delta nfa) q (Just s);
          q1s'  = (delta nfa) q Nothing;
          qss   = concatMap (\q1 -> paths' nfa q1 ss) (Set.toList q1s);
          qss'  = concatMap (\q1 -> paths' nfa q1 w) (Set.toList q1s');
          qss'' = filter (/=[]) (qss++qss');

accept :: Ord q => NFAe q s -> [s] -> Bool
accept nfa ss = not . null $ paths nfa ss 

union :: (Ord q1, Ord q2, Ord s) =>
    NFAe q1 s -> NFAe q2 s -> NFAe (q1, q2) s
union nfa1 nfa2 = 
    let states'  = Set.fromList [(q1,q2)| q1<-q1s, q2<-q2s]
            where q1s = Set.toList $ states nfa1
                  q2s = Set.toList $ states nfa2
        symbols' = (symbols nfa1) `Set.union` (symbols nfa1)
        start'   = ((start nfa1), (start nfa2)) 
        final'   = 
            Set.filter (\(q1,q2)-> Set.member q1 f1s || Set.member q2 f2s) states'
                where f1s = final nfa1
                      f2s = final nfa2
        delta' (q1,q2) s = Set.fromList [(q1, q2)| q1<-q1s, q2<-q2s]
            where q1s = Set.toList $ delta nfa1 q1 s
                  q2s = Set.toList $ delta nfa2 q2 s
    in NFAe states' symbols' delta' start' final'

cat :: (Ord q, Ord s) => NFAe q s -> NFAe q s -> NFAe q s
cat nfa1 nfa2 = 
    let states'  = q1s `Set.union` q2s
        symbols' = (symbols nfa1) `Set.union` (symbols nfa2)
        start'   = start nfa1
        final'   = final nfa2
        delta' q s | Set.member q q1s && Set.notMember q f1s = delta1 q s
                   | Set.member q f1s && isJust s = delta1 q s
                   | Set.member q f1s && isNothing s = 
                        (start nfa2) `Set.insert` delta1 q s
                   | otherwise = delta2 q s
                        where f1s = final nfa1
                              delta1 = delta nfa1
                              delta2 = delta nfa2
    in NFAe states' symbols' delta' start' final'
        where q1s = states nfa1
              q2s = states nfa2
                       
star :: (Ord s, Ord q, Eq q, Enum q) => NFAe q s -> NFAe q s
star nfa = 
    let states' = start' `Set.insert` states nfa
        symbols' = symbols nfa 
        start' = succ $ Set.findMax $ states nfa
        final' = start' `Set.insert` states nfa
        delta' q s | Set.member q qs && Set.notMember q fs = delta nfa q s
                   | Set.member q fs && isNothing s = delta nfa q s
                   | Set.member q fs && isJust s = 
                       start' `Set.insert` delta nfa q s 
                   | q == start' && isNothing s = Set.singleton $ start nfa
                   | otherwise = Set.empty
                       where qs = states nfa
                             fs = final nfa
    in NFAe states' symbols' delta' start' final'

intersection :: (Ord q1, Ord q2, Ord s) =>
     NFAe q1 s -> NFAe q2 s -> NFAe (q1, q2) s
intersection nfa1 nfa2 = 
    let states'  = Set.fromList [(q1, q2)| q1<-q1s, q2<-q2s]
            where q1s = Set.toList $ states nfa1
                  q2s = Set.toList $ states nfa2
        symbols' = (symbols nfa1) `Set.union` (symbols nfa2)
        start'   = ((start nfa1), (start nfa2))
        final'   = Set.fromList [(f1,f2)| f1<-f1s, f2<-f2s]
                where f1s = Set.toList $ final nfa1
                      f2s = Set.toList $ final nfa2
        delta' (q1,q2) s = Set.fromList [(q1, q2)| q1<-q1s, q2<-q2s]
            where q1s = Set.toList $ delta nfa1 q1 s
                  q2s = Set.toList $ delta nfa2 q2 s
    in NFAe states' symbols' delta' start' final'
