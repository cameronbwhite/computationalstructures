import NFA

import Data.Set (Set)
import qualified Data.Set as Set

n1 = NFA {
    states = Set.fromList [1, 2, 3, 4],
    symbols = Set.fromList [0, 1],
    delta = delta',
    start = 1,
    final = Set.fromList [4]}
        where delta' 1 0 = Set.singleton 4;
              delta' 1 1 = Set.fromList [4,2];
              delta' 2 1 = Set.singleton 3;
              delta' 3 1 = Set.singleton 4;
              delta' 4 0 = Set.singleton 4;
              delta' 4 1 = Set.singleton 1;
              delta' _ _ = Set.empty;

