type NFAe q s = {
   states  :: [q],
   symobls :: [s],
   delta   :: q -> Maybe s -> [q],
   start   :: q,
   final   :: [q]
   }

path nfae ss | qs =:= deltabar nfae q0 ss = qs
              where q0 = (nfae :> start)
                    qs = q0 : xs ++ [f]
                    f = foldl1 (?) (nfae :> final)
                    xs free

deltabar :: NFAe q s -> q -> [s] -> [q]
deltabar _ q [] = [q]
deltabar nfae q (s:ss) = q : deltabar nfae q' ss
    where q' = (foldl1 (?) $ delta q Nothing) ?
               (foldl1 (?) $ delta q $ Just s)
          delta = (nfae :> delta) 
