import DFA

d1 = { states  := "ABC", 
       symbols := "01", 
       delta   := delta', 
       start   := 'A', 
       final   := "C"  }
delta' 'A' '0' = 'B'
delta' 'A' '1' = 'C'
delta' 'B' '0' = 'C'
delta' 'B' '1' = 'D'
