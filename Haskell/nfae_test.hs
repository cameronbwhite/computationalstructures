import NFAe

import Data.Set (Set)
import qualified Data.Set as Set

ne1 = NFAe {
    states = Set.fromList [1, 2, 3, 4],
    symbols = Set.fromList [0, 1],
    delta = delta',
    start = 1,
    final = Set.fromList [4]}
        where delta' 1 (Just 0) = Set.singleton 4;
              delta' 1 Nothing = Set.singleton 3;
              delta' 3 (Just 0) = Set.singleton 4;
              delta' _ _ = Set.empty;

ne2 = makeNFAe "ABCD" "01" delta2 'A' "D"
delta2 'A' (Just '0') = Set.singleton 'D';
delta2 'A' Nothing    = Set.singleton 'C';
delta2 'C' (Just '0') = Set.singleton 'D';
delta2 _ _            = Set.empty;
