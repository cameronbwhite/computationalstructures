ComputationalStructures
=======================

Code related to my CS311 Computational Structures class 

```haskell
> import DFA

>import Data.Set (Set)
>import qualified Data.Set as Set

> d1 = DFA {
    states = Set.fromList [0, 1, 2],
    symbols = Set.fromList [0, 1],
    delta = delta',
    start = 0,
    final = Set.fromList [2]}
        where delta' 0 0 = 0;
              delta' 0 1 = 1;
              delta' 1 0 = 2;
              delta' 1 1 = 0;
              delta' 2 0 = 1;
              delta' 2 1 = 2

> d1
(Q=[0,1,2],Σ=[0,1],δ=[(0,0,0),(0,1,1),(1,0,2),(1,1,0),(2,0,1),(2,1,2)],q=0,F=[2])

> accept d1 [0]
False
> accept d1 [1, 0, 1]
True
> accept d1 [1, 0, 1, 1]
True
> accept d1 [1, 0, 1, 1, 0]
False 

> path d1 [0]
[]
> path d1 [1, 0, 1]
[0,1,2,2]
> path d1 [1, 0, 1, 1]
[0,1,2,2,2]
> path d1 [1, 0, 1, 1, 0]
[]
```

```haskell
> import NFA

> import Data.Set (Set)
> import qualified Data.Set as Set

> n1 = NFA {
    states = Set.fromList [1, 2, 3, 4],
    symbols = Set.fromList [0, 1],
    delta = delta',
    start = 1,
    final = Set.fromList [4]}
        where delta' 1 0 = Set.singleton 4;
              delta' 1 1 = Set.fromList [4,2];
              delta' 2 1 = Set.singleton 3;
              delta' 3 1 = Set.singleton 4;
              delta' 4 0 = Set.singleton 4;
              delta' 4 1 = Set.singleton 1;
              delta' _ _ = Set.empty;
> n1
(Q=[1,2,3,4],Σ=[0,1],δ=[(1,0,[4]),(1,1,[4,2]),(2,0,[]),(2,1,[3]),(3,0,[]),(3,1,[4]),(4,0,[4]),(4,1,[1])],q=1,F=[4])

> path n1 [1,1,1,1,1]
[1,4,1,4,1,4]
> path n1 [0,1]
[]

> paths n1 [1,1,1,1,1]
[[1,4,1,4,1,4],[1,4,1,2,3,4],[1,2,3,4,1,4]]
> paths n1 [0,1]
[]

> accept n1 [1,1,1,1,1]
True
> accept n1 [0,1]
False
```

```haskell
> import NFAe

> import Data.Set (Set)
> import qualified Data.Set as Set

> ne1 = NFAe {
    states = Set.fromList [1, 2, 3, 4],
    symbols = Set.fromList [0, 1],
    delta = delta',
    start = 1,
    final = Set.fromList [4]}
        where delta' 1 (Just 0) = Set.singleton 4;
              delta' 1 Nothing = Set.singleton 3;
              delta' 3 (Just 0) = Set.singleton 4;
              delta' _ _ = Set.empty;

> path ne1 [0]
[1,4]
> path ne1 [1]
[]

> paths ne1 [0]
[[1,4],[1,3,4]]
> paths ne1 [1]
[]

> accept ne1 [0]
True
> accept ne1 [1]
False
```
