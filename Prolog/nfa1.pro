% N is the NFA (Q, E, D, q, F)
% N = ({1,2,3,4}, {0,1},
%      {(1,0,{1}),(1,1,{1,2}),(2,0,3),(2,e,3),
%       (3,1,{4}),(4,0,{4}),(4,1,{4})},
%      1, {4})

parse(L) :-
    start(S),
    trans(S,L).

trans(X, []) :-
    final(X).
trans(X, [A|As]) :-
    delta(X, A, X1),
    trans(X1, As).
trans(X, As) :-
    delta(X, e, X1),
    trans(X1, As).

delta(1, 0, 1).
delta(1, 1, 1).
delta(1, 1, 2).
delta(2, 0, 3).
delta(2, e, 3).
delta(3, 1, 4).
delta(4, 0, 4).
delta(4, 1, 4).
 
start(1).
final(4).
