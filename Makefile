# Copyright © 2013 Cameron White

HC = ghc
HFLAGS = -Wall -O2
TARGETS = DFA NFA NFAe GNFA RegExp

all: $(TARGETS)

DFA: DFA.hs
	$(HC) $(HFLAGS) --make $^
	
NFA: NFA.hs
	$(HC) $(HFLAGS) --make $^

NFAe: NFAe.hs
	$(HC) $(HFLAGS) --make $^

GNFA: GNFA.hs
	$(HC) $(HFLAGS) --make $^

RegExp: RegExp.hs
	$(HC) $(HFLAGS) --make $^

clean:
	-rm -f *.o *.hi
