-- Copyright © 2013 Cameron White 
-- All rights reserved.
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the project nor the names of its contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
module Converters (fromDFAtoNFA) where 

import DFA (DFA(DFA))
import NFA (NFA(NFA))
import NFAe (NFAe(NFAe))
import Data.Maybe (fromJust, isNothing)
import Data.Tuple (swap)
import Data.List (subsequences, find)
import Data.Set (Set)
import qualified Data.Set as Set

fromDFAtoNFA :: DFA q s -> NFA q s
fromDFAtoNFA (DFA states symbols delta start final) = 
    (NFA states symbols delta' start final)
    where delta' q s = Set.singleton $ delta q s

fromNFAtoNFAe :: NFA q s -> NFAe q s
fromNFAtoNFAe (NFA states symbols delta start final) =
    (NFAe states symbols delta' start final)
    where delta' q (Just s) = delta q s

fromNFAtoDFA :: (Ord q) => NFA q s -> DFA (Set q) s
fromNFAtoDFA (NFA states symbols delta start final) = 
    let states'    = Set.fromList $ 
                     map (Set.fromList) $ subsequences $ Set.toList states
        start'     = Set.singleton start 
        final'     = Set.filter (Set.isSubsetOf final) states'
        delta' q s = Set.unions [ delta q' s | q' <- Set.toList q ]

    in DFA states' symbols delta' start' final'
