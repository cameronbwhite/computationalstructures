import DFA

import Data.Set (Set)
import qualified Data.Set as Set
import Converters

d1 = DFA {
    states = Set.fromList [0, 1, 2],
    symbols = Set.fromList [0, 1],
    delta = delta',
    start = 0,
    final = Set.fromList [2]}
        where delta' 0 0 = 0;
              delta' 0 1 = 1;
              delta' 1 0 = 2;
              delta' 1 1 = 0;
              delta' 2 0 = 1;
              delta' 2 1 = 2
