module DFA where 

type DFA q s = {
    states  :: [q],
    symbols :: [s],
    delta   :: q -> s -> q,
    start   :: q,
    final   :: [q]
}

path dfa ss | qs =:= deltabar dfa q0 ss = qs
              where q0 = (dfa :> start)
                    qs = q0 : xs ++ [f]
                    f = foldl1 (?) (dfa :> final)
                    xs free

deltabar _   q []     = [q]
deltabar dfa q (s:ss) = q : deltabar dfa q' ss
    where q' = (dfa :> delta) q s
