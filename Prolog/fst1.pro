% N is the FST (Q, E, G, D, q)
% N = ({1,2}, {0,1,2}, {0,1},
%      {(1,0,1,0),(1,1,1,0),(1,2,2,1),
%       (2,0,1,0),(2,1,2,1),(2,2,2,1)}, 
%      1)

parse(I,O) :-
    start(S),
    trans(S,I,O).

trans(X, [], []).
trans(X, [I|Is], [O|Os]) :-
    delta(X, I, X1, O),
    trans(X1, Is, Os).

start(1).
 
%   s0, i, s1, o
delta(1, 0, 1, 0).
delta(1, 1, 1, 0).
delta(1, 2, 2, 1).
delta(2, 0, 1, 0).
delta(2, 1, 2, 1).
delta(2, 2, 2, 1).
